from django.contrib import admin
from employee_register.models import Employee, Position


# Register your models here.

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('fullname', 'emp_code', 'mobile', 'salary', 'position')
    list_per_page = 10
    search_fields = ('fullname', 'emp_code', 'position')


class PositionAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Position, PositionAdmin)
